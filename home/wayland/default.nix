{ pkgs
, ...
}: 
{
  imports = [
    ./sway.nix
    ./waybar.nix
  ];
  
  programs = {
    swaylock.enable = true;
  };

  home.packages = with pkgs; [
    wlr-randr
    wl-clipboard
  ];

  home.pointerCursor = {
    gtk.enable = true;
    package = pkgs.vanilla-dmz;
    name = "Vanilla-DMZ";
    size = 22;
  };
}

