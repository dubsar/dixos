{lib, ...}:
with lib; let
  name = "alex";
  mod4 = "Mod4";
  terminal = "alacritty";
  criteria = "eDP-1";

  # navigation
  left = "h";
  down = "j";
  up = "k";
  right = "l";

  bg = ./hiroshige-two-men-by-a-gate-in-the-mountains.jpg;
in {
  wayland = {
    windowManager = {
      sway = {
        enable = true;
        config = {
          # startup = [
          #   { command = "lock"; }
          #   { command = "autotiling"; }
          #   #{ command = "${udiskie}/bin/udiskie -s --appindicator --menu-update-workaround -f ${pkgs.pcmanfm}/bin/pcmanfm"; }
          #   { command = "import-gsettings"; always = true; }
          #   { command = "mako"; }
          # ];
          defaultWorkspace = "1";
          output."*" = {bg = "${bg} fill";};
          input = {
            "*" = {
              xkb_layout = "us,it,gr";
              xkb_variant = "altgr-intl,,polytonic";
              xkb_options = "grp:rctrl_toggle";
            };
          };
          modifier = "${mod4}";
          terminal = "${terminal}";
          bars = [
            {
              command = "waybar";
              position = "top";
              colors = {
                statusline = "#ffffff";
                background = "#323232";
                #inactive_workspace = "#32323200 #32323200 #5c5c5c";
              };
            }
          ];
          focus = {
            forceWrapping = false;
            followMouse = false;
          };
          fonts = {
            names = ["Roboto"];
            size = 8.0;
          };
          window = {
            border = 0;
            titlebar = false;
            commands = [
              {
                command = "floating enable, sticky enable";
                criteria.title = "Picture-in-Picture";
              }
              {
                command = "floating enable, sticky enable";
                criteria.title = ".*Sharing Indicator.*";
              }
            ];
          };
          keybindings = mkOptionDefault {
            # rofi: menu
            "${mod4}+d" = "exec rofi -show drun";
            # rofi: clipboard manager
            "${mod4}+c" = "exec cliphist list | rofi -dmenu | cliphist decode | wl-copy";
            # rofi: bluetooth
            "${mod4}+y" = "exec rofi-bluetooth";
            # rofi: password store
            "${mod4}+e" = "exec rofi-pass";
            # rofi: emoji
            "${mod4}+m" = "exec rofi -modi emoji -show emoji";
            # pick color
            "${mod4}+n" = "exec wl-color-picker clipboard";
            # mirror screen
            "${mod4}+o" = "exec wl-present mirror";

            # modes
            "${mod4}+g" = "mode recording";
            "${mod4}+i" = "mode randr";
            "${mod4}+p" = "mode printscreen";
            "${mod4}+r" = "mode resize";
            "${mod4}+u" = "mode audio";
            "${mod4}+x" = "mode session";
            "${mod4}+z" = "mode apps";

            "${mod4}+period" = "workspace next";
            "${mod4}+comma" = "workspace prev";

            "${mod4}+1" = "workspace number 1";
            "${mod4}+2" = "workspace number 2";
            "${mod4}+3" = "workspace number 3";
            "${mod4}+4" = "workspace number 4";
            "${mod4}+5" = "workspace number 5";
            "${mod4}+6" = "workspace number 6";
            "${mod4}+7" = "workspace number 7";
            "${mod4}+8" = "workspace number 8";
            "${mod4}+9" = "workspace number 9";

            "${mod4}+Shift+period" = "move container to workspace next; workspace next";
            "${mod4}+Shift+comma" = "move container to workspace prev; workspace prev";

            "${mod4}+Shift+1" = "move container to workspace number 1";
            "${mod4}+Shift+2" = "move container to workspace number 2";
            "${mod4}+Shift+3" = "move container to workspace number 3";
            "${mod4}+Shift+4" = "move container to workspace number 4";
            "${mod4}+Shift+5" = "move container to workspace number 5";
            "${mod4}+Shift+6" = "move container to workspace number 6";
            "${mod4}+Shift+7" = "move container to workspace number 7";
            "${mod4}+Shift+8" = "move container to workspace number 8";
            "${mod4}+Shift+9" = "move container to workspace number 9";

            "${mod4}+${left}" = "focus left";
            "${mod4}+${down}" = "focus down";
            "${mod4}+${up}" = "focus up";
            "${mod4}+${right}" = "focus right";

            "${mod4}+Ctrl+${left}" = "move workspace to output left";
            "${mod4}+Ctrl+${down}" = "move workspace to output down";
            "${mod4}+Ctrl+${up}" = "move workspace to output up";
            "${mod4}+Ctrl+${right}" = "move workspace to output right";

            "${mod4}+Shift+${left}" = "move left";
            "${mod4}+Shift+${down}" = "move down";
            "${mod4}+Shift+${up}" = "move up";
            "${mod4}+Shift+${right}" = "move right";

            # audio control
            "XF86AudioRaiseVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+";
            "XF86AudioLowerVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-";
            "XF86AudioMute" = "exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";

            # mic control
            "${mod4}+XF86AudioRaiseVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SOURCE@ 5%+";
            "${mod4}+XF86AudioLowerVolume" = "exec wpctl set-volume @DEFAULT_AUDIO_SOURCE@ 5%-";
            "${mod4}+XF86AudioMute" = "exec wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";

            # player control
            "XF86AudioPlay" = "exec mpc toggle";
            "XF86AudioPrev" = "exec mpc prev; exec mpc toggle; exec mpc toggle";
            "XF86AudioNext" = "exec mpc next; exec mpc toggle; exec mpc toggle";
            "XF86AudioStop" = "exec mpc stop";

            # brightness
            "XF86MonBrightnessUp" = "exec light -A 2";
            "XF86MonBrightnessDown" = "exec light -U 2";
          };
          modes = {
            apps = {
              Escape = "mode default";
              Return = "mode default";
              "b" = "exec blender, mode default";
              "d" = "exec dbeaver, mode default";
              "f" = "exec firefox, mode default";
              "g" = "exec gimp, mode default";
              "i" = "exec inkscape, mode default";
              "k" = "exec krita, mode default";
              "l" = "exec libreoffice, mode default";
              "o" = "exec orange-canvas, mode default";
              "q" = "exec qutebrowser, mode default";
              "r" = "exec rstudio, mode default";
              "s" = "exec spyder, mode default";
              "t" = "exec telegram-desktop, mode default";
              "v" = "exec virt-manager, mode default";
              "Shift+f" = "exec firefox -private, mode default";
              "Shift+q" = "exec qutebrowser --target private-window, mode default";
            };
            audio = {
              # audio = "launch: [i]input [o]output";
              Escape = "mode default";
              Return = "mode default";
              "i" = "exec rofi-pulse-select source, mode default";
              "o" = "exec rofi-pulse-select sink, mode default";
            };
            printscreen = {
              # printscreen = "launch: [1]save-area [2]save-all [3]copy-area [4]copy-all";
              Escape = "mode default";
              Return = "mode default";
              "1" = ''
                exec sleep 1.0; exec grim -g "$(slurp -d)" "$(xdg-user-dir PICTURES)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.png)" | wl-copy -t image/png, mode default'';
              "2" = ''
                exec sleep 1.0; exec grim "$(xdg-user-dir PICTURES)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.png)" | wl-copy -t image/png, mode default'';
              "3" = ''
                exec sleep 1.0; exec grim -g "$(slurp -d)" - | wl-copy -t image/png, mode default'';
              "4" = "exec sleep 1.0; exec grim - | wl-copy -t image/png, mode default";
            };
            #
            # manage outputs: https://sr.ht/~emersion/wlr-randr/
            #
            randr = {
              Escape = "mode default";
              Return = "mode default";
              "1" = "exec wlr-randr --output ${criteria} --mode 1024x768, mode default";
              "2" = "exec wlr-randr --output ${criteria} --mode 1366x768, mode default";
              "3" = "exec wlr-randr --output ${criteria} --mode 1920x1080, mode default";
            };
            recording = {
              # printscreen = "launch:
              # [1]area-with-audio [2]full-with-audio;
              # [3]area-without-audio [4]full-without-audio;
              # [0]stop-record";
              Escape = "mode default";
              Return = "mode default";
              "1" = ''
                exec sleep 1.0; exec wl-screenrec --low-power=off --filename="$(xdg-user-dir VIDEOS)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.mp4)" --geometry "$(slurp -d)" --audio, mode default'';
              "2" = ''
                exec sleep 1.0; exec wl-screenrec --low-power=off --filename="$(xdg-user-dir VIDEOS)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.mp4)" --audio, mode default'';
              "3" = ''
                exec sleep 1.0; exec wl-screenrec --low-power=off --filename="$(xdg-user-dir VIDEOS)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.mp4)" --geometry "$(slurp -d)", mode default'';
              "4" = ''
                exec sleep 1.0; exec wl-screenrec --low-power=off --filename="$(xdg-user-dir VIDEOS)/$(date +%Y%m%d_%Hh%Mm%Ss_@${name}.mp4)", mode default'';
              "0" = "exec sleep 1.0; exec pkill --signal INT wl-screenrec, mode default";
            };
            resize = {
              Escape = "mode default";
              Return = "mode default";
              "${down}" = "resize grow height 5 px or 5 ppt";
              "${left}" = "resize shrink width 5 px or 5 ppt";
              "${right}" = "resize grow width 5 px or 5 ppt";
              "${up}" = "resize shrink height 5 px or 5 ppt";
            };
            session = {
              # session = launch:
              # [h]ibernate [p]oweroff [r]eboot
              # [s]uspend [l]ockscreen log[o]ut
              Escape = "mode default";
              Return = "mode default";
              "h" = "exec systemctl hibernate, mode default";
              "p" = "exec systemctl poweroff, mode default";
              "r" = "exec systemctl reboot, mode default";
              "s" = "exec systemctl suspend, mode default";
              "l" = "exec swaylock, mode default";
              "o" = "exec swaymsg exit, mode default";
            };
          };
        };
      };
    };
  };
}
