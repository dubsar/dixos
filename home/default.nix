{ pkgs,
  ...
}:
let
  name = "alex";
  ver = "24.05";
in {
  imports = [
    ./programs
    ./services
    ./wayland
    ./xdg.nix
  ];

  home = {
    username = "alex";
    homeDirectory = "/home/${name}";
    stateVersion = "${ver}";
  };
}

