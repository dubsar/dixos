{ ...
}:
{
  programs = {
    mpv = {
      enable = true;
      config = {
        force-window = "yes";
        fullscreen = "no";
        profile = "gpu-hq";
        ytdl-format = "bestvideo+bestaudio";
      };
    };
  };
}
