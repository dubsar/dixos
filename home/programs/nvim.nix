{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [

    vimPlugins.nvim-treesitter
    vimPlugins.nvim-treesitter.withAllGrammars

    # xclip
    # wl-clipboard

    nixfmt-classic
    shfmt
    stylua
    marksman
    markdownlint-cli

    lua-language-server

    #ghcid
    #haskellPackages.stack
    #haskellPackages.ghc
    #haskellPackages.cabal-install
    #haskellPackages.haskell-language-server
    #haskellPackages.stylish-haskell
    #haskellPackages.cabal-fmt
    #haskellPackages.fourmolu

    haskell-language-server
    #vimPlugins.haskell-tools-nvim
  ];

  programs.neovim = {
    enable = true;
    defaultEditor = true;

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
  };

  home.file = {
    ".config/nvim" = {
      source = ./nvim;
      recursive = true;
    };
  };
}
