{ pkgs, ... }: {
  programs = {
    password-store = {
      enable = true;
      package = pkgs.pass-wayland.withExtensions (exts: [
        exts.pass-checkup
        exts.pass-genphrase
        exts.pass-import
        exts.pass-otp
        exts.pass-tomb
        exts.pass-update
      ]);
    };
  };
}
