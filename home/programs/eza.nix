{...}: {
  programs = {
    eza = {
      enable = true;
      #extraOptions = [ "--group-directories-first" "--header" ];
      enableNushellIntegration = true;
      git = true;
      icons = "auto";
    };
  };
}
