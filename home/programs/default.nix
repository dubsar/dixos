{pkgs, ...}: {
  imports = [
    ./alacritty.nix
    ./aria2.nix
    ./bat.nix
    ./bottom.nix
    ./direnv.nix
    ./emacs
    ./eza.nix
    ./firefox.nix
    ./git.nix
    ./helix.nix
    ./htop.nix
    ./mpv.nix
    ./ncmpcpp.nix
    ./obs.nix
    ./nvim.nix
    ./pandoc.nix
    ./password.nix
    ./ripgrep.nix
    ./rofi.nix
    ./vscode.nix
    ./wofi.nix
    ./yt-dlp.nix
    ./zathura.nix
  ];

  programs = {
    bash.enable = true;
    fd.enable = true;
    fzf.enable = true;
    gpg.enable = true;
    imv.enable = true;
    jq.enable = true;
    lazygit.enable = true;

    ## disabled
    java.enable = false;
    nushell.enable = false;
  };
}
