{ pkgs
, ...
}:
{
  home.file = { ".config/emacs/init.el" = { source = ./init.el; }; };
  home.file = { ".config/emacs/config.org" = { source = ./config.org; }; };
  programs.emacs = {
    enable = true;
    package = pkgs.emacs30-pgtk;
    extraPackages =
      epkgs:
      (with epkgs; [ treesit-grammars.with-all-grammars ])
      ++ (with epkgs.elpaPackages; [
        #     #  auctex
        #     #  company
        #     #  gcmh
        #     #  rec-mode
        #     #  yasnippet
      ])
      ++ (with epkgs.melpaStablePackages; [
        doom-themes
        flycheck
        notmuch
        #     #  deadgrep
        #     #  devil
        #     #  direnv
        #     #  editorconfig
        #     #  elixir-mode
        #     #  fuel
        #     #  go-mode
        #     #  haskell-mode
        #     #  highlight-indentation
        magit
        #     #  markdown-mode
        #     #  nix-mode
        pdf-tools
        #     #  puppet-mode
        #     #  raku-mode
        #     #  slime
        #     #  smartparens
        #     #  yaml-mode
        #     #  zoxide
      ])
      ++ (with epkgs.melpaPackages; [
        vterm
        which-key
        #     #  avy
        #     #  bqn-mode
        #     #  toml-mode
        #     #  ttl-mode
        #     #  zig-mode
      ]);
  };

  services.emacs = {
    enable = true;
    #defaultEditor = true;
  };
}

