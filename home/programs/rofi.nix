{ config
, pkgs
, ...
}:
let
  name = "alex";

  # colors
  plasma_blue = "#3DAEE9";
  paper_white = "#FCFCFC";
  shade_black = "#232629";

  blue_1 = plasma_blue;
  light_1 = paper_white;
  dark_8 = shade_black;

in
{
  programs = {
    rofi = {
      enable = true;
      font = "Roboto 8";
      extraConfig = {
        case-sensitive = false;
        display-drun = "Apps:";
        modi = [ "drun" "run" ];
        show-icons = false;
      };
      pass = {
        enable = true;
        package = pkgs.rofi-pass-wayland;
        stores = [ "/home/${name}/.pass" ];
      };
      plugins = with pkgs; [
        rofi-pass-wayland
        rofi-emoji
      ];
      theme =
        let mkLiteral = config.lib.formats.rasi.mkLiteral;
        in {
          "*" = {
            bg = mkLiteral dark_8;
            fg = mkLiteral light_1;
            ac = mkLiteral blue_1;
          };

          "#window" = {
            location = mkLiteral "center";
            width = mkLiteral "50%";
          };

          "#prompt" = {
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
          };

          "#textbox-prompt-colon" = {
            text-color = mkLiteral "@fg";
          };

          "#entry" = {
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
            blink = mkLiteral "true";
          };

          "#inputbar" = {
            children = mkLiteral "[ prompt, entry ]";
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
            padding = mkLiteral "5px";
          };

          "#listview" = {
            background-color = mkLiteral "@bg";
            columns = mkLiteral "1";
            lines = mkLiteral "5";
            cycle = mkLiteral "false";
            dynamic = mkLiteral "true";
          };

          "#mainbox" = {
            background-color = mkLiteral "@bg";
            border = mkLiteral "3px";
            border-color = mkLiteral "@ac";
            children = mkLiteral "[ inputbar, listview ]";
            padding = mkLiteral "10px";
          };

          "#element" = {
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
            padding = mkLiteral "5px";
          };

          "#element-icon" = {
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
            size = mkLiteral "32px";
          };

          "#element-text" = {
            background-color = mkLiteral "@bg";
            text-color = mkLiteral "@fg";
            padding = mkLiteral "5px";
          };

          "#element selected" = {
            border = mkLiteral "3px";
            border-color = mkLiteral "@ac";
          };
        }
      ;
    };
  };
}
