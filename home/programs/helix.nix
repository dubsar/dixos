{pkgs ? import <nixpkgs> {}, ...}: {
  programs = {
    helix = {
      enable = true;
      defaultEditor = false;
      settings = {
        theme = "default";

        editor = {
          bufferline = "always";
          cursorline = true;
          cursorcolumn = true;
          line-number = "relative";
          true-color = true;

          lsp = {
            display-messages = true;
          };
        };
      };

      languages = with pkgs; {
        language = [
          {
            name = "python";
            language-servers = ["pyright" "ruff" "pylsp"];
            auto-format = true;
            formatter = {
              command = "black";
              args = ["--line-length" "88" "--quiet" "-"];
            };
          }
          {
            name = "nix";
            auto-format = true;
            formatter = {
              command = "${alejandra}/bin/alejandra";
              args = ["-qq"];
            };
          }
        ];
        language-server = {
          ruff = {
            command = "ruff-lsp";
            config = {
              settings = {
                args = ["--ignore" "E501"];
              };
            };
          };
          pyright = {
            config = {
              python = {
                analysis = {
                  typeCheckingMode = "basic";
                };
              };
            };
          };
        };
      };
    };
  };
}
