{ ...
}:
let
  plasma_blue = "#3DAEE9";
  icon_yellow = "#FDBC4B";
  beware_orange = "#F67400";
  icon_red = "#DA4453";
  paper_white = "#FCFCFC";
  icon_gray = "#4D4D4D";
  charcoal_gray = "#31363B";
  shade_black = "#232629";

  blue_1 = plasma_blue;
  yellow_1 = icon_yellow;
  orange_1 = beware_orange;
  red_2 = icon_red;
  light_1 = paper_white;
  dark_5 = icon_gray;
  dark_7 = charcoal_gray;
  dark_8 = shade_black;

in
{
  programs = {
    zathura = {
      enable = true;
      options = {
        adjust-open = "width";
        font = "Roboto 8";
        guioptions = "none";
        pages-per-row = 1;
        scroll-step = 50;
        completion-bg = blue_1;
        completion-fg = light_1;
        completion-group-bg = blue_1;
        completion-group-fg = light_1;
        completion-highlight-bg = blue_1;
        completion-highlight-fg = light_1;
        default-bg = dark_7;
        default-fg = light_1;
        highlight-active-color = orange_1;
        highlight-color = dark_5;
        highlight-fg = orange_1;
        index-active-bg = blue_1;
        index-active-fg = light_1;
        index-bg = dark_7;
        index-fg = light_1;
        inputbar-bg = dark_8;
        inputbar-fg = light_1;
        notification-bg = dark_8;
        notification-error-bg = dark_8;
        notification-error-fg = red_2;
        notification-fg = light_1;
        notification-warning-bg = dark_8;
        notification-warning-fg = yellow_1;
        recolor-darkcolor = light_1;
        recolor-lightcolor = dark_7;
        render-loading-bg = dark_7;
        render-loading-fg = light_1;
        scrollbar-bg = dark_7;
        scrollbar-fg = light_1;
        statusbar-bg = dark_8;
        statusbar-fg = light_1;
        selection-clipboard = "clipboard";
        statusbar-home-tilde = true;
        scroll-page-aware = true;
        window-title-basename = true;
      };
    };
  };
}
