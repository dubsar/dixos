{ ...
}:
let
  email = "scotti.alessandro@gmail.com";
  fullname = "Alessandro Scotti <scotti.alessandro@gmail.com>";
in
{
  programs = {
    git = {
      enable = true;
      userEmail = "${email}";
      userName = "${fullname}";
      lfs = {
        enable = true;
      };
      ignores = [
        "*~"
        "*.swp"
      ];
      extraConfig = {
        init = {
          defaultBranch = "main";
        };
        pull = {
          rebase = true;
        };
        core = {
          whitespace = "trailing-space,space-before-tab";
        };
      };
    };
  };
}
