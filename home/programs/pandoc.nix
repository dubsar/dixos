{ ...
}:
let
  fullname = "Alessandro Scotti";
in
{
  programs = {
    pandoc = {
      enable = false;
      defaults = {
        metadata = {
          author = "${fullname}";
        };
        pdf-engine = "xelatex";
        citeproc = true;
      };
    };
  };
}
