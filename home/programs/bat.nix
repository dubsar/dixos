{...}: {
  programs = {
    bat = {
      enable = true;
      config = {
        italic-text = "always";
        paging = "auto";
        tabs = "2";
      };
    };
  };
}
