#
# https://github.com/emersion/mako
#

{ ...
}:
let
  plasma_blue = "#3DAEE9";
  icon_blue = "#1D99F3";
  paper_white = "#FCFCFC";
  shade_black = "#232629";

  blue_1 = plasma_blue;
  blue_3 = icon_blue;
  light_1 = paper_white;
  dark_8 = shade_black;

in
{
  services = {
    mako = {
      enable = true;
      backgroundColor = dark_8;
      borderColor = blue_1;
      borderSize = 3;
      defaultTimeout = 10000; # 5s
      font = "Roboto 8";
      margin = "30";
      padding = "5";
      progressColor = "over ${blue_3}";
      textColor = light_1;
    };
  };
}
