{ pkgs,
  ...
}:
{
  services = {
    swayidle = {
      enable = true;
      timeouts = [
        { timeout = 600; command = "${pkgs.sway}/bin/swaymsg 'output * dpms off'"; resumeCommand = "${pkgs.sway}/bin/swaymsg 'output * dpms on'"; }
      ];
    };
  };
}

