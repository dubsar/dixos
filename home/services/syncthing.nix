#
# https://syncthing.net/
#

{ ...
}:
{
  services = {
    syncthing = {
      enable = false;
    };
  };
}
