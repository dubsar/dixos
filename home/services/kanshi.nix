# https://sr.ht/~emersion/kanshi/
#
{...}: {
  services = {
    kanshi = {
      enable = true;
      systemdTarget = "sway-session.target";
      settings = [
        {
          profile = {
            name = "tadino";
            outputs = [
              {
                adaptiveSync = true;
                criteria = "eDP-1";
                # mode = "1920x1080@60Hz";
                scale = 1.0;
                status = "enable";
                position = "0,0";
              }
              {
                adaptiveSync = true;
                criteria = "HDMI-A-1";
                mode = "1920x1200@100Hz";
                scale = 1.0;
                status = "enable";
                position = "1920,0";
              }
            ];
          };
        }
        {
          profile = {
            name = "undocked";
            outputs = [
              {
                adaptiveSync = true;
                criteria = "eDP-1";
                # mode = "1920x1080@60Hz";
                scale = 1.0;
                status = "enable";
              }
            ];
          };
        }
      ];
    };
  };
}
