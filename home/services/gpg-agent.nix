{ ...
}:
{
  services = {
    gpg-agent = {
      enable = true;
      enableFishIntegration = true;
      enableSshSupport = true;
      #pinentryPackage = "gnome3"; # gtk2, gnome3, qt
    };
  };
}
