#
# https://github.com/MusicPlayerDaemon/MPD
#

{ ...
}:
let
  name = "alex";
in
{
  services = {
    mpd = {
      enable = true;
      musicDirectory = "/home/${name}/music";
    };
  };
}
