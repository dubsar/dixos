{ ...
}:
{
  imports = [
    ./batsignal.nix
    ./gpg-agent.nix
    ./kanshi.nix
    ./keyring.nix
    ./mako.nix
    ./mpd.nix
    ./swayidle.nix
    ./udiskie.nix
    ./xsettingsd.nix

  ];
  
  services.ssh-agent.enable = true;

  ## disabled
  services.cliphist.enable = false;
}
