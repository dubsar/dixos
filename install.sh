set -e

sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./metal/disko-config.nix
sudo nixos-generate-config --no-filesystems --root /mnt
sudo cp /mnt/etc/nixos/hardware-configuration.nix ./metal
sudo rm -rf /mnt/etc/nixos/*
sudo cp -r ./* /mnt/etc/nixos/
sudo nixos-install --root /mnt --flake '/mnt/etc/nixos#nixos'

