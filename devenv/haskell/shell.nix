# full "~/myproject/shell.nix" file
let
  myNixPkgs = import <nixpkgs> { overlays = [ myNixPkgsOverlay ]; };

  myNixPkgsOverlay = (nixSelf: nixSuper: {
    myHaskellPackages = nixSelf.haskellPackages.override (oldHaskellPkgs: {
      overrides =
        nixSelf.lib.composeExtensions (oldHaskellPkgs.overrides or (_: _: { }))
        myHaskellPkgsOverlay;
    });
  });

  myHaskellPkgsOverlay = (hSelf: hSuper: {
    # "myproject" is the first part of the "myproject.cabal" project definition file
    haskell-starter = hSelf.callCabal2nix "haskell-starter" ./. { };
  });

  myDevTools = with myNixPkgs; [ cabal-install haskellPackages.ghcid ];

  myShellHook = ''
    alias repl="cabal new-repl"
  '';
in myNixPkgs.myHaskellPackages.haskell-starter.env.overrideAttrs (oldEnv: {
  nativeBuildInputs = oldEnv.nativeBuildInputs ++ myDevTools;
  shellHook = myShellHook;
})

