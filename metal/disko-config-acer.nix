{
  disko.devices = {
    disk.ssd = {
      type = "disk";
      device = "/dev/nvme0n1";
      content = {
        type = "gpt";
        partitions = {
          ESP = {
            type = "EF00";
            size = "500M";
            content = {
              type = "filesystem";
              format = "vfat";
              mountOptions = [ "umask=0077" "defaults" ];
              mountpoint = "/boot";
            };
          };
          swap = {
            size = "36G";
            content = { type = "swap"; };
          };
          root = {
            size = "100%";
            content = {
              type = "filesystem";
              format = "ext4";
              mountpoint = "/";
              mountOptions = [ "defaults" ];
            };
          };
        };
      };
    };
    disk.hd = {
      type = "disk";
      device = "/dev/sda";
      content = {
        type = "gpt";
        partitions = {
          nix = {
            size = "200G";
            content = {
              type = "filesystem";
              format = "ext4";
              mountpoint = "/nix";
              mountOptions = [ "noatime" ];
            };
          };
          node = {
            size = "100%";
            content = {
              type = "filesystem";
              format = "ext4";
              mountpoint = "/node";
              mountOptions = [ "noatime" "defaults" ];
            };
          };
        };
      };
    };
  };
}

