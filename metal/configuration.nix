{ ...
}:
{
  imports =
    [
      ./hardware-configuration.nix
      ./disko-config.nix
    ];

  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "24.05";
}

