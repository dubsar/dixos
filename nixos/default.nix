{...}: {
  imports = [
    ./hardware
    ./programs
    ./services
    ./system
    ./virtual
  ];
}
