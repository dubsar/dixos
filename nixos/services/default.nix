{...}: {
  imports = [
    ./audio.nix
    ./dbus.nix
    ./earlyoom.nix
    ./greetd.nix
    ./printing.nix
    ./xserver.nix
  ];

  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  services.blueman.enable = true;
  services.fstrim.enable = true;
  services.openssh.enable = true;
  services.timesyncd.enable = true;
  services.udev.enable = true;
  services.udisks2.enable = true;

  # disabled
  services.pcscd.enable = false;
  services.power-profiles-daemon.enable = false;
  services.qemuGuest.enable = false;
}

