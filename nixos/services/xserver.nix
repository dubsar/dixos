{ pkgs, ... }: {
  services = {
    displayManager = { sessionPackages = [ pkgs.sway ]; };
    libinput = { enable = true; };
    xserver = {
      enable = true;
      desktopManager = { xterm = { enable = false; }; };
      excludePackages = with pkgs; [ xterm ];
      videoDrivers = [ "amdgpu" "radeon" "nouveau" "modesetting" "fbdev" ];
      xkb = {
        layout = "us,it,gr";
        variant = "altgr-intl,,polytonic";
        #options = "grp:shift_caps_toggle,compose:rwin";
      };
    };
  };
}
