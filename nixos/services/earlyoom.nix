#
# https://github.com/rfjakob/earlyoom
#

{ ... }: {
  services = {
    earlyoom = {
      enable = false;
      freeMemThreshold = 5;
    };
  };
}
