{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    alacritty
    bat
    dua
    fd
    ffmpeg-full
    gcc
    ripgrep
    wget
  ];

  programs = {
    command-not-found.enable = true;
    dconf.enable = true;
    gnupg.agent.enable = true;
    gnupg.agent.enableSSHSupport = true;
    light.enable = true;
    mtr.enable = true;
    system-config-printer.enable = true;
    virt-manager.enable = true;
  };

  imports = [
    ./fish.nix
  ];
}
