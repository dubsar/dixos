{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    fzf
    grc
    fishPlugins.pure
    fishPlugins.done
    fishPlugins.fzf-fish
    fishPlugins.forgit
    fishPlugins.hydro
    fishPlugins.grc
  ];

  programs.fish = {
    enable = true;
    useBabelfish = true;
    interactiveShellInit = ''
      set fish_greeting # Disable greeting

      fish_vi_key_bindings
      function fish_user_key_bindings
        bind -M insert \c] accept-autosuggestion execute
      end

      set --universal pure_enable_nixdevshell true
      set --universal pure_enable_single_line_prompt true
      set --universal pure_shorten_prompt_current_directory_length 1

      # colored man output
      # from http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
      setenv LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
      setenv LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
      setenv LESS_TERMCAP_me \e'[0m'           # end mode
      setenv LESS_TERMCAP_se \e'[0m'           # end standout-mode
      setenv LESS_TERMCAP_so \e'[38;5;246m'    # begin standout-mode - info box
      setenv LESS_TERMCAP_ue \e'[0m'           # end underline
      setenv LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline

      setenv FZF_DEFAULT_COMMAND 'fd --type file --follow'
      setenv FZF_CTRL_T_COMMAND 'fd --type file --follow'
      setenv FZF_DEFAULT_OPTS '--height 20%'

    '';
  };
}
