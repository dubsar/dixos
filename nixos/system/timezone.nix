{ ... }:
let
  timezone = "Europe/Rome";
in
{
  time.timeZone = "${timezone}";
}
