{ lib
, ...
}:
let
  hostname = "nixos";
in
{
  networking = {
    hostName = "${hostname}";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };
}

