{ ... }: {
  networking = {
    firewall = {
      enable = true;
      allowPing = false;
      allowedTCPPorts = [
        22
        80
        8384
        22000
      ];
      allowedUDPPorts = [
        22000
        21027
      ];
      connectionTrackingModules = [
        "amanda"
        "ftp"
        "h323"
        "irc"
        "netbios_sn"
        "pptp"
        "sane"
        "sip"
        "snmp"
        "tftp"
      ];
    };
  };
}
