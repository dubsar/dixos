{ ... }:
let
  ver = "24.05";
in
{
  imports = [
    ./boot.nix
    ./documentation.nix
    ./environment.nix
    ./firewall.nix
    ./fonts.nix
    ./locale.nix
    ./network.nix
    ./nix.nix
    ./power.nix
    ./security.nix
    ./systemd.nix
    ./timezone.nix
    ./upgrade.nix
    ./users.nix
    ./xdg.nix
    ./zram.nix
  ];

  hardware.bluetooth.enable = true;

  system = {
    stateVersion = "${ver}";
  };
}
