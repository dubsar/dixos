{ ...
}: {
  security.polkit.enable = true;
  environment = {
    # etc = {
    #   "greetd/environments".text = ''
    #     sway
    #     fish
    #     bash
    #   '';
    # };
    interactiveShellInit = ''
      alias btm='btm -m'
      alias lt='eza -T'
    '';
    sessionVariables = {
      MOZ_ENABLE_WAYLAND = "1";
      MOZ_USE_XINPUT2 = "1";
      QT_QPA_PLATFORM = "wayland";
      QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
      SDL_VIDEODRIVER = "wayland";
      XDG_CURRENT_DESKTOP = "sway";
      XDG_SESSION_DESKTOP = "sway";
      XDG_SESSION_TYPE = "wayland";

      # CLUTTER_BACKEND=wayland;
      # DESKTOP_SESSION=sway;
      # LIBSEAT_BACKEND=logind;
      # WLR_DRM_DEVICES=/dev/dri/card0;
      # XCURSOR_SIZE = "24";
      # XKB_DEFAULT_LAYOUT=us;
      # _JAVA_AWT_WM_NONREPARENTING=1;
    };
  };
}
