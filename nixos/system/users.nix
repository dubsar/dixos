{ pkgs
, ...
}:
let
  name = "alex";
  fullname = "Alessandro Scotti";
in
{
  users = {
    defaultUserShell = pkgs.fish;
    users = {
      os = {
        isNormalUser = true;
        description = "NixOS Configuration";
        extraGroups = [ "wheel" ];
      };
      ${name} = {
        isNormalUser = true;
        description = "${fullname}";
        uid = 1000;
        extraGroups = [
          "audio"
          "disk"
          "input"
          "libvirtd"
          "lp"
	  "scanner"
          "systemd-journal"
          "users"
          "video"
          "wheel"
        ];
      };
    };
  };
}
