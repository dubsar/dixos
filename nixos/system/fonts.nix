{ pkgs
, ...
}: {
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      # alegreya
      # alice
      # amiri
      # caladea
      # cantarell-fonts
      # carlito
      # comfortaa
      corefonts
      # fira
      # fira-code
      # font-awesome
      # hackgen-font
      # ibm-plex
      # inconsolata
      # inter
      # iosevka
      jetbrains-mono
      lato
      liberation-sans-narrow
      liberation_ttf
      libertinus
      material-icons
      # mona-sans
      # montserrat
      # nerdfonts
      # unicode-emoji
      twitter-color-emoji
      # overpass
      # pecita
      # recursive
      roboto
      roboto-mono
      roboto-serif
      roboto-slab
      source-code-pro
      source-sans
      source-sans-pro
      source-serif
      source-serif-pro

      font-awesome
      powerline-fonts
      powerline-symbols
      # (nerdfonts.override { fonts = [ "NerdFontsSymbolsOnly" ]; })
      nerd-fonts.symbols-only
    ];
    fontconfig = {
      defaultFonts = {
        monospace = [ "Roboto Mono" "Source Code Pro" ];
        serif = [ "Roboto Serif" "Source Serif Pro" ];
        sansSerif = [ "Roboto" "Source Sans Pro" ];
        emoji = [ "Twitter Color Emoji" ];
      };
    };
  };
}
