{ pkgs,
  ...
}:{
  hardware = {
    sane = {
      enable = true;
      extraBackends = [ pkgs.sane-airscan ];
      brscan4 = {
        enable = false;
      };
    };
  };
}
