{
  pkgs,
  ...
}: {
  hardware = {
    graphics = {
      # driSupport = true;
      # driSupport32Bit = true;
      extraPackages = with pkgs; [
        amdvlk
        intel-media-driver
        intel-vaapi-driver
        libvdpau-va-gl
        vaapiIntel
        vaapiVdpau
      ];
    };
  };
}
