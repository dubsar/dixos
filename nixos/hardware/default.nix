{
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ./graphics.nix
    ./platform.nix
    ./processor.nix
    ./sane.nix
  ];
}
